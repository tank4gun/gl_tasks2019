//
// Created by daniil on 03.04.19.
//

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/LightInfo.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "perlin_noise.h"

#include <iostream>
#include <vector>

std::vector<std::vector<double> > heights;

MeshPtr makeSurface(unsigned int N = 100, int size = 5, int h = 10)
{
  PerlinNoise pn(237);

  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::vec2> texcoords;

  std::vector<double> height(N * size + 1, 0.0);
  heights.resize(N * size + 1, height);
  std::cout << heights.size() << std::endl;

  for( unsigned int i = 0; i < N * size; i++ )
  {
    for( unsigned int j = 0; j < N * size; j++ )
    {
      double noise_x = pn.noise((float)i / N, (float)j / N, 0) * h;
      double noise_y = pn.noise((float)(i + 1) / N, (float)j / N, 0) * h;
      double noise_z = pn.noise((float)i / N, (float)(j + 1) / N, 0) * h;
      glm::vec3 x = glm::vec3((float)i / N * size * 4, (float)j / N * size * 4, noise_x * 4);
      glm::vec3 y = glm::vec3((float)(i + 1) / N * size * 4, (float)j / N * size * 4, noise_y * 4);
      glm::vec3 z = glm::vec3((float)i / N * size * 4, (float)(j + 1) / N * size * 4, noise_z * 4);

      vertices.push_back(x);
      vertices.push_back(y);
      vertices.push_back(z);

      normals.push_back(glm::normalize(x));
      normals.push_back(glm::normalize(y));
      normals.push_back(glm::normalize(z));

      texcoords.push_back(glm::vec2((float)i / N / size, (float)j / N / size));
      texcoords.push_back(glm::vec2((float)(i + 1) / N / size, (float)j / N / size));
      texcoords.push_back(glm::vec2((float)i / N / size, (float)(j + 1) / N / size));

      heights[i][j] = noise_x * 4;
      heights[i + 1][j] = noise_y * 4;
      heights[i][j + 1] = noise_z * 4;

      noise_x = pn.noise((float)(i + 1) / N, (float)(j + 1) / N, 0) * h;

      glm::vec3 a = glm::vec3((float)(i + 1) / N * size * 4, (float)(j + 1) / N * size * 4, noise_x * 4);
      glm::vec3 b = glm::vec3((float)(i + 1) / N * size * 4, (float)j / N * size * 4, noise_y * 4);
      glm::vec3 c = glm::vec3((float)i / N * size * 4, (float)(j + 1) / N * size * 4, noise_z * 4);

      vertices.push_back(a);
      vertices.push_back(b);
      vertices.push_back(c);

      normals.push_back(glm::normalize(a));
      normals.push_back(glm::normalize(b));
      normals.push_back(glm::normalize(c));

      texcoords.push_back(glm::vec2((float)(i + 1) / N / size, (float)(j + 1) / N / size));
      texcoords.push_back(glm::vec2((float)(i + 1) / N / size, (float)j / N / size));
      texcoords.push_back(glm::vec2((float)i / N / size, (float)(j + 1) / N / size));

      heights[i + 1][j + 1] = noise_x * 4;

    }
  }

  //----------------------------------------

  DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

  DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

  DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

  MeshPtr mesh = std::make_shared<Mesh>();
  mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
  mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
  mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
  mesh->setPrimitiveType(GL_TRIANGLES);
  mesh->setVertexCount(vertices.size());

  std::cout << "Surface is created with " << vertices.size() << " vertices\n";

  return mesh;
}


class SampleApplication : public Application {
  public:
    MeshPtr _surface;

    ShaderProgramPtr _shader;
    MeshPtr _marker;
    MeshPtr _sphere;
    MeshPtr _backgroundCube;
    ShaderProgramPtr _markerShader, _skyboxShader;

    TexturePtr _worldTexture, _worldTexture1, _worldTexture2, _worldTexture3, _worldTexture4, _cubeTex;
    GLuint _sampler1;
    GLuint _cubeTexSampler;

    LightInfo _light;
    CameraMoverPtr _freeCam, _surfaceCam;

    //Координаты источника света
    float _x = 3.0;
    float _y = 0.0;
    float _z = 1.0f;
    int _currentCam = 0;

    int outerLevel = 8;
    int innerLevel = 8;
    bool dynamicLevel = false;
    int maxTessLevel = 0;

    void makeScene() override {
      Application::makeScene();

      glGetIntegerv(GL_MAX_TESS_GEN_LEVEL, &maxTessLevel);

      _surface = makeSurface();
      //_surface->setPrimitiveType(GL_PATCHES);

      _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, -10.0f, -10.0f)));

      _freeCam = std::make_shared<FreeCameraMover>();
      _surfaceCam = std::make_shared<SurfaceCameraMover>(heights);
      _cameraMover = _freeCam;
      _marker = makeSphere(0.1f);
      _backgroundCube = makeCube(1.0f);

      _shader = std::make_shared<ShaderProgram>("596GlebovData3/texture.vert", "596GlebovData3/texture.frag");

//      _shader = std::make_shared<ShaderProgram>();
//
//      ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
//      vs->createFromFile("596GlebovData3/texture.vert");
//      _shader->attachShader(vs);
//
//      ShaderPtr tcs = std::make_shared<Shader>(GL_TESS_CONTROL_SHADER);
//      tcs->createFromFile("596GlebovData3/tess.tesc");
//      _shader->attachShader(tcs);
//
//      ShaderPtr tes = std::make_shared<Shader>(GL_TESS_EVALUATION_SHADER);
//      tes->createFromFile("596GlebovData3/tess.tese");
//      _shader->attachShader(tes);
//
//      ShaderPtr fs = std::make_shared<Shader>(GL_FRAGMENT_SHADER);
//      fs->createFromFile("596GlebovData3/texture.frag");
//      _shader->attachShader(fs);
//      _shader->linkProgram();

      _markerShader = std::make_shared<ShaderProgram>("596GlebovData3/marker.vert", "596GlebovData3/marker.frag");
      _skyboxShader = std::make_shared<ShaderProgram>("596GlebovData3/skybox.vert", "596GlebovData3/skybox.frag");

      //Инициализация значений переменных освщения

      _light.position = glm::vec3(_x, _y, _z);
      _light.ambient = glm::vec3(0.2, 0.2, 0.2);
      _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
      _light.specular = glm::vec3(1.0, 1.0, 1.0);
      //=========================================================
      //Загрузка и создание текстур
      _worldTexture = loadTexture("596GlebovData3/circle.png", SRGB::YES);
      _worldTexture1 = loadTexture("596GlebovData3/sand.jpg");
      _worldTexture2 = loadTexture("596GlebovData3/grass.jpg");
      _worldTexture3 = loadTexture("596GlebovData3/gravel1.jpg");
      _worldTexture4 = loadTexture("596GlebovData3/snow.jpg");
      _cubeTex = loadCubeTextureHDR("596GlebovData3/CubeHDR");

      //=========================================================
      //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
      glGenSamplers(1, &_sampler1);
      glSamplerParameteri(_sampler1, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glSamplerParameteri(_sampler1, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glGenSamplers(1, &_cubeTexSampler);
      glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

      glEnable(GL_DEPTH_TEST);
      glEnable(GL_POLYGON_OFFSET_FILL);
    }

    void updateGUI() override {
      Application::updateGUI();

      ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
      if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Light")) {
          ImGui::ColorEdit3("ambient", const_cast<float *>(glm::value_ptr(_light.ambient)));
          ImGui::ColorEdit3("diffuse", const_cast<float *>(glm::value_ptr(_light.diffuse)));
          ImGui::ColorEdit3("specular", const_cast<float *>(glm::value_ptr(_light.specular)));

          ImGui::SliderFloat("x", &_x, 0.1f, 25.0f);
          ImGui::SliderFloat("y", &_y, 0.1f, 25.0f);
          ImGui::SliderFloat("z", &_z, 0.0f, 10.0f);
        }
        if (ImGui::CollapsingHeader("Camera")) {
          ImGui::RadioButton("Space", &_currentCam, 0);
          ImGui::RadioButton("Ground", &_currentCam, 1);
        }


        ImGui::SliderInt("outer tess", &outerLevel, 1, maxTessLevel);
        ImGui::SliderInt("inner tess", &innerLevel, 1, maxTessLevel);

        ImGui::Checkbox("dynamic", &dynamicLevel);
      }
      ImGui::End();
    }

    void draw() override {
      if (_currentCam == 0 && _cameraMover != _freeCam) {
        _cameraMover = _freeCam;
        _freeCam->saveData(_surfaceCam.get());
      }
      if (_currentCam == 1 && _cameraMover != _surfaceCam) {
        _cameraMover = _surfaceCam;
        _surfaceCam.get()->saveData(_freeCam.get());
      }
      //Получаем текущие размеры экрана и выставлям вьюпорт
      int width, height;
      glfwGetFramebufferSize(_window, &width, &height);
      glViewport(0, 0, width, height);

      //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      // glEnable(GL_LINE_SMOOTH);
      {
        _skyboxShader->use();

        glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]); //Извлекаем из матрицы вида положение виртуальный камеры в мировой системе координат

        _skyboxShader->setVec3Uniform("cameraPos", cameraPos);
        _skyboxShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _skyboxShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Для преобразования координат в текстурные координаты нужна специальная матрица
        glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        _skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _cubeTexSampler);
        _cubeTex->bind();
        _skyboxShader->setIntUniform("cubeTex", 0);

        glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

        _backgroundCube->draw();

        glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
      }
      // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glEnable(GL_LINE_SMOOTH);
      //Подключаем шейдер
      _shader->use();

      //Загружаем на видеокарту значения юниформ-переменные: время и матрицы

      // _shader->setMat4Uniform("modelMatrix", glm::mat4(1.0f));

      _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
      _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
      _light.position = glm::vec3(_x, _y, _z);
      glm::vec3 sunDir = - _light.position * glm::vec3(999,999,999);
      _light.position = glm::vec3(10.0f, 10.0f, 5.0f) - sunDir;
      glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

      _shader->setVec3Uniform("light.pos", lightPosCamSpace);
      _shader->setVec3Uniform("light.La", _light.ambient);
      _shader->setVec3Uniform("light.Ld", _light.diffuse);
      _shader->setVec3Uniform("light.Ls", _light.specular);

//      _shader->setIntUniform("outerLevel", outerLevel);
//      _shader->setIntUniform("innerLevel", innerLevel);
//
//      _shader->setIntUniform("dynamicLevel", dynamicLevel ? 1 : 0);

      glEnable(GL_BLEND);

      GLuint textureUnitForDiffuseTex1 = 0;
      glActiveTexture(GL_TEXTURE0);
      glBindSampler(0, _sampler1);
      _worldTexture->bind();
      _shader->setIntUniform("diffuseTex", 0);

      glActiveTexture(GL_TEXTURE1);
      glBindSampler(1, _sampler1);
      _worldTexture1->bind();
      _shader->setIntUniform("diffuseTex1", 1);

      glActiveTexture(GL_TEXTURE2);
      glBindSampler(2, _sampler1);
      _worldTexture2->bind();
      _shader->setIntUniform("diffuseTex2", 2);

      glActiveTexture(GL_TEXTURE3);
      glBindSampler(3, _sampler1);
      _worldTexture3->bind();
      _shader->setIntUniform("diffuseTex3", 3);

      glActiveTexture(GL_TEXTURE4);
      glBindSampler(4, _sampler1);
      _worldTexture4->bind();
      _shader->setIntUniform("diffuseTex4", 4);

      //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
      _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
      _shader->setMat3Uniform("normalToCameraMatrix",
                              glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface->modelMatrix()))));

//      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//
//      glEnable(GL_CULL_FACE);
//      glFrontFace(GL_CCW);
//      glCullFace(GL_BACK);
//
//      glPatchParameteri(GL_PATCH_VERTICES, 3);
      _surface->draw();
//      glDisable(GL_CULL_FACE);


      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glDisable(GL_LINE_SMOOTH);
      //Рисуем маркер для источника света
      {
        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix",
                                      _camera.projMatrix * _camera.viewMatrix
                                          * glm::translate(glm::mat4(1.0f), _light.position));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
//        _marker->draw();
      }
      glBindSampler(0, 0);
      glUseProgram(0);
    }
};

int main()
{
  SampleApplication app;
  app.start();

  return 0;
}