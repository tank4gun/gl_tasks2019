/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;
uniform sampler2D diffuseTex1;
uniform sampler2D diffuseTex2;
uniform sampler2D diffuseTex3;
uniform sampler2D diffuseTex4;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
float shininess = 128.0;

const float snow_blick = 100.0;
const float sand_blick = 50.0;
const float grass_blick = 10.0;
const float gravel_blick = 70.0;

float sum_rgb = 0;

void main()
{
    sum_rgb = texture(diffuseTex, texCoord).r + texture(diffuseTex, texCoord).g + texture(diffuseTex, texCoord).b + texture(diffuseTex, texCoord).a;
	vec3 diffuseColor = (texture(diffuseTex1, texCoord).rgb * texture(diffuseTex, texCoord).r + texture(diffuseTex2, texCoord).rgb * texture(diffuseTex, texCoord).g + texture(diffuseTex3, texCoord).rgb * texture(diffuseTex, texCoord).b + texture(diffuseTex4, texCoord).rgb * texture(diffuseTex, texCoord).a) / sum_rgb;
    shininess = sand_blick * texture(diffuseTex, texCoord).r + grass_blick * texture(diffuseTex, texCoord).g + gravel_blick * texture(diffuseTex, texCoord).b + snow_blick * texture(diffuseTex, texCoord).a;
	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		color += light.Ls * Ks * blinnTerm;
	}

	fragColor = vec4(color, 1.0);
}
